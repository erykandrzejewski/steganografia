#!/usr/bin/env python3

from PIL import Image
import sys

print('Witaj w programie, który pozwala ukryć tajny obrazek wewnątrz obrazka jawnego')
print('Ważne, aby obrazek jawny był zapisany w odcieniach szarości (8-bitowy)')
print('Obrazek tajny musi być zapisany jako obrazek monochromatyczny (1-bitowy)')
print('Obydwa obrazki muszą być identycznego rozmiaru!')

apparentImageName = input('Podaj, proszę, nazwę pliku z jawnym obrazkiem: ')
apparentImage = Image.open(apparentImageName)

secretImageName = input('Podaj, proszę, nazwę pliku z tajnym obrazkiem: ')
secretImage = Image.open(secretImageName)

if apparentImage.size != secretImage.size:
    print('Obydwa obrazki muszą mieć identyczny rozmiar!')
    sys.exit(1)

size = apparentImage.size
resultImage = Image.new('L', size)

apparentImagePixels = apparentImage.load()
secretImagePixels = secretImage.load()
resultImagePixels = resultImage.load()

for y in range(size[0]):
    for x in range(size[1]):
        temp = apparentImagePixels[y, x][0] & 0b11111110  # Zero the least significant bit
        resultImagePixels[y, x] = temp | secretImagePixels[y, x]

print('Pomyślnie wygenerowano obrazek')
resultImageName = input('Podaj nazwę pliku, do którego zapisać obrazek: ')

resultImage.save(resultImageName)
