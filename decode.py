#!/usr/bin/env python3

from PIL import Image

print('Witaj w programie dekodującym obrazek')

encodedImageName = input('Podaj, proszę, nazwę pliku z zakodowanym obrazkiem: ')

encodedImage = Image.open(encodedImageName)
encodedPixels = encodedImage.load()

size = encodedImage.size

decodedImage = Image.new('1', size)
decodedPixels = decodedImage.load()

for y in range(size[0]):
    for x in range(size[1]):
        decodedPixels[y, x] = not bool(encodedPixels[y, x] & 1)

print('Obrazek został pomyślnie zdekodowany. Oto on')
decodedImage.show()

decodedImageName = input('Podaj, proszę, nazwę pliku, do którego mam zapisać zdekodowany obrazek: ')
decodedImage.save(decodedImageName)
